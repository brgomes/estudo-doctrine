<?php

namespace DoctrineNaPratica\Model\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\EventArgs;
use Doctrine\ORM\Event;
use Doctrine\ORM\Events;

/**
 * Classe base que vai ouvir todos os eventos relacionados as entidades
 * 
 */
class EntitySubscriber implements EventSubscriber
{

    /**
     * Lista de entidades a serem monitoradas
     * @var array
     */
    private $listenedEntities = array(
        'DoctrineNaPratica\Model\Progress',
    );
    
    /**
     * Lista de eventos a serem monitorados
     * @var  array
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::onFlush,
            Events::preUpdate,
            Events::postUpdate,
            Events::postPersist
        );
    }

    /**
     * Verifica se a entidade sendo alterada é uma das monitoradas
     * 
     * @var boolean 
     */
    protected function isListenedTo($entity)    
    {
        $entityClass = get_class($entity);
        if (in_array($entityClass, $this->listenedEntities)) {
            return true;
        }

        return false;
    }

    /**
     * Verifica o progresso geral do usuário no curso
     * @param  Course $course Curso
     * @param  User $user   Usuário
     * @param  EntityManager $em     EntityManager
     * @return int         Percentual de progresso
     */
    private function getCourseProgress($course, $user, $em)
    {
        $lessons = $course->getLessonCollection();
        $lessonIds = array();
        foreach ($lessons as $l) {
            $lessonIds[] = $l->getId();
        }
        
        //busca o progresso do aluno no curso
        $qb = $em->createQueryBuilder();
        $qb->select('SUM(p.percent) AS percent')
           ->from('DoctrineNapratica\Model\Progress', 'p')
           ->where($qb->expr()->andX(
                $qb->expr()->in('p.lesson', ':lessonIds'),
                $qb->expr()->eq('p.user', ':user')
            ))
           ->setParameters(array('lessonIds' => $lessonIds, 'user' => $user));

        $query = $qb->getQuery();
        $percent = $query->getSingleScalarResult();
        return $percent;
    }

    /**
     * Método que será executado após a inclusão de uma entidade
     * @param  EventArgs $args Argumentos do evento
     */
    public function postPersist(EventArgs $args)
    {
        //só faz o cálculo caso a entidade sendo salva é a monitorada
        if ( ! $this->isListenedTo($args->getEntity())) return;
        
        $e = $args->getEntity();
        $em = $args->getEntityManager();
        $course = $e->getLesson()->getCourse();
        $user = $e->getUser();
        $numberOfLessons = count($e->getLesson()->getCourse()->getLessonCollection());
        $courseProgress = $this->getCourseProgress($course, $user, $em);

        //verifica se progresso do aluno no curso é igual ao total do curso
        if (($numberOfLessons * 100) == $courseProgress) {
            //pega o Enrollment do usuário
            $enrollment = $em->getRepository('DoctrineNaPratica\Model\Enrollment')
                             ->findOneBy(array('user' => $user, 'course' => $course));
            $certificationCode = md5($user->getId() . $course->getId());
            $enrollment->setCertificationCode($certificationCode);
            $em->persist($enrollment);
            $em->flush();
        }        
    }
    
    /**
     * Método que será executado antes da atualização das entidades
     * @param  EventArgs $args Argumentos do evento
     */
    public function preUpdate(EventArgs $args)
    {

    }

    /**
     * MÃ©todo que será executado após a inclusão ou alteração
     * @param  EventArgs $args Argumentos do evento
     */
    public function onFlush(EventArgs $args)
    {   

    }

    /**
     * Método que será executado após a atualização das entidades
     * @param  EventArgs $args Argumentos do evento
     */
    public function postUpdate(EventArgs $args)
    {

    }

}
