<?php

namespace DoctrineNaPratica\Model\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use DoctrineNaPratica\Model\User as ModelUser;

/**
 * Cria alunos para popular a base
 */
class User implements FixtureInterface
{

    private $data = array(
        0 => array(
            'name' => 'Elton Minetto',
            'login' => 'eminetto',
            'email' => 'eminetto@coderockr.com',
        ),
        1 => array(
            'name' => 'Steve Jobs',
            'login' => 'steve',
            'email' => 'jobs@coderockr.com',
        ),
        2 => array(
            'name' => 'Bill Gates',
            'login' => 'bill',
            'email' => 'bill@coderockr.com',
        ),
    );

    public function load(ObjectManager $manager)
    {
        foreach ($this->data as $d) {
            $user = new ModelUser;
            $user->setName($d['name']);
            $user->setLogin($d['login']);
            $user->setEmail($d['email']);
            $manager->persist($user);
        }
        $manager->flush();
    }

}
