<?php

namespace DoctrineNaPratica\Model;

use DoctrineNaPratica\Test\TestCase;
use DoctrineNaPratica\Model\User;
use DoctrineNaPratica\Model\Course;
use DoctrineNaPratica\Model\Lesson;
use DoctrineNaPratica\Model\Progress;
use DoctrineNaPratica\Model\Enrollment;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @group Model
 */
class QbTest extends TestCase
{

    //busca todos os usuários
    public function testUser()
    {
        $userA = $this->buildUser('userA');
        $userB = $this->buildUser('userB');
        
        $this->getEntityManager()->persist($userA);
        $this->getEntityManager()->persist($userB);
        $this->getEntityManager()->flush();

        $qb = $this->em->createQueryBuilder();
        $this->assertEquals('Doctrine\ORM\QueryBuilder', get_class($qb));

        $qb->select('u')
           ->from('DoctrineNapratica\Model\User', 'u');

        $query = $qb->getQuery();
        $users = $query->getResult();
        $this->assertEquals(2, count($users));

        $this->assertEquals($userA->getId(), $users[0]->getId());
        $this->assertEquals($userB->getId(), $users[1]->getId());
        
        $this->assertInstanceOf(get_class($userA), $users[0]);
    }   

    //buscar lições que tenham 100 de progresso
    public function testUserLesson()
    {
        $userA = $this->buildUser('UserA');

        $course = new Course;
        $course->setName('PHP');
        $course->setDescription('Curso de PHP');
        $course->setValue(100);
        $course->setTeacher($userA);

        $lessonA = new Lesson;
        $lessonA->setName('Arrays');
        $lessonA->setDescription('Aula sobre Arrays');
        $lessonA->setCourse($course);

        $lessonB = new Lesson;
        $lessonB->setName('Datas');
        $lessonB->setDescription('Aula sobre Datas');
        $lessonB->setCourse($course);

        $lessonCollection = new ArrayCollection;
        $lessonCollection->add($lessonA);
        $lessonCollection->add($lessonB);
        $userA->setLessonCollection($lessonCollection);

        $enrollment = new Enrollment;
        $enrollment->setUser($userA);
        $enrollment->setCourse($course);

        $progressA = new Progress;
        $progressA->setPercent(100);
        $progressA->setUser($userA);
        $progressA->setLesson($lessonA);
        $progressA->setCreated(\DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s')));

        $progressB = new Progress;
        $progressB->setPercent(90);
        $progressB->setUser($userA);
        $progressB->setLesson($lessonB);
        $progressB->setCreated(\DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s')));

        $this->getEntityManager()->persist($userA);
        $this->getEntityManager()->persist($enrollment);
        $this->getEntityManager()->persist($progressA);
        $this->getEntityManager()->persist($progressB);
        $this->getEntityManager()->flush();

        $qb = $this->em->createQueryBuilder();

        $qb->select('p')
           ->from('DoctrineNapratica\Model\Progress', 'p')
           ->innerJoin('p.lesson', 'l')
           ->innerJoin('l.course', 'c')
           ->where('p.percent = 100');

        $query = $qb->getQuery();

        $result = $query->getResult();
        
        $this->assertEquals(1, count($result));

        $this->assertEquals($userA->getId(), $result[0]->getUser()->getId());
        $this->assertEquals($lessonA->getId(), $result[0]->getLesson()->getId());
    }

    //testa a dql dos Users com parametros
    public function testUserParameters()
    {
        $userA = $this->buildUser('userA');
        $userB = $this->buildUser('userB');
        
        $this->getEntityManager()->persist($userA);
        $this->getEntityManager()->persist($userB);
        $this->getEntityManager()->flush();

        $qb = $this->em->createQueryBuilder();
        $qb->select('u')
           ->from('DoctrineNapratica\Model\User', 'u')
           ->where('u.login = :login or u.email = :email')
           ->setParameters(array('login' => 'userA', 'email' => 'userA@domain.com'));

        $query = $qb->getQuery();
        $users = $query->getResult(); 

        $this->assertEquals(1, count($users));

        $this->assertEquals($userA->getId(), $users[0]->getId());
        
        $this->assertInstanceOf(get_class($userA), $users[0]);

        //outra forma de escrever a query
        $qb = $this->em->createQueryBuilder();
        $qb->select('u')
           ->from('DoctrineNapratica\Model\User', 'u')
           ->where($qb->expr()->orX(
                $qb->expr()->eq('u.login', ':login'),
                $qb->expr()->eq('u.email', ':email')
            ))
           ->setParameters(array('login' => 'userA', 'email' => 'userA@domain.com'));

        $query = $qb->getQuery();
        $users = $query->getResult(); 

        $this->assertEquals(1, count($users));

        $this->assertEquals($userA->getId(), $users[0]->getId());
        
        $this->assertInstanceOf(get_class($userA), $users[0]);
    }

    //cria um User
    private function buildUser($login)
    {
        $user = new User;
        $user->setName($login . ' name');
        $user->setLogin($login);
        $user->setEmail($login . '@domain.com');
        $user->setAvatar($login . '.png');

        return $user;
    }

    //cria um Course
    private function buildCourse($courseName)
    {
        $teacher = $this->buildUser();
        //login é unique
        $teacher->setLogin('jobs'.$courseName);

        $course = new Course;
        $course->setName($courseName);
        $course->setDescription('Curso de PHP');
        $course->setValue(100);
        $course->setTeacher($teacher);

        return $course;
    }

    //testa os hydrators, limit e order
    public function testHydrators()
    {
        $users = array();
        for($i=0; $i<30; $i++) {
            $users[$i] = $this->buildUser('user' . $i);
            $this->getEntityManager()->persist($users[$i]);
        }
        $this->getEntityManager()->flush();

        $qb = $this->em->createQueryBuilder();

        //retorna os 10 primeiros ordenados por name
        $qb->select('u')
           ->from('DoctrineNapratica\Model\User', 'u')
           ->setMaxResults(10)
           ->orderBy('u.name', 'ASC');

        $query = $qb->getQuery();
        $result = $query->getResult(); 

        $this->assertEquals(10, count($result));

        //retorna os dados no formato de um array de objetos (é o padrão)
        $result = $query->getResult(); 
        $this->assertInstanceOf(get_class($users[0]), $result[0]);
        $this->assertEquals($users[0]->getId(), $result[0]->getId());

        //retorna os dados na forma de um array de arrays
        $result = $query->getArrayResult();
        $this->assertTrue(is_array($result[0]));
        $this->assertEquals($users[0]->getId(), $result[0]['id']);

        //O alias da entidade é adicionado ao indice do array. 
        $result = $query->getScalarResult();
        $this->assertTrue(is_array($result[0]));
        $this->assertEquals($users[0]->getId(), $result[0]['u_id']);

        //se a consulta retorna apenas um elemento podemos usar o single_scalar
        $qb = $this->em->createQueryBuilder();
        $qb->select('u.login')
           ->from('DoctrineNapratica\Model\User', 'u')
           ->setMaxResults(1);
        $query = $qb->getQuery();
        $result = $query->getSingleScalarResult();
        $this->assertEquals($users[0]->getLogin(), $result);
    }

}
